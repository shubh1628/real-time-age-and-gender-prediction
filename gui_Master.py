from subprocess import call
import tkinter as tk
import tkinter as tk
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from tensorflow.keras.layers import Layer

from PIL import Image, ImageTk
from tkinter import ttk
from sklearn.decomposition import PCA
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from tkvideo import tkvideo
'''import detection_emotion_practice as validate'''

# This Python 3 environment comes with many helpful anal,ytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from tensorflow.keras.layers import Conv2D
#from keras.layers.convolutional import Convolution2D
#from keras.layers.convolutional import MaxPooling2D
from tensorflow.keras.layers import MaxPooling2D
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
from keras import backend as K


root = tk.Tk()
root.title("Automatic Age And Gender Detection System")

w, h = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry("%dx%d+0+0" % (w, h))
# ++++++++++++++++++++++++++++++++++++++++++++

image2 = Image.open('D:/REAL TIME AGE GENDER/REAL TIME AGE GENDER/REAL TIME AGE GENDER/REAL TIME AGE GENDER/REAL TIME/img3.jpg')

image2 = image2.resize((w, h), Image.ANTIALIAS)

background_image = ImageTk.PhotoImage(image2)


background_label = tk.Label(root, image=background_image)
background_label.image = background_image

background_label.place(x=0, y=10) 

 # , relwidth=1, relheight=1)
lbl = tk.Label(root, text="REAL TIME AGE AND GENDER PREDICTION", font=('times', 35,' bold '), height=1, width=55,bg="violet Red",fg="Black")
lbl.place(x=0, y=0)
# _+++++++++++++++++++++++++++++++++++++++++++++++++++++++



def call_file():
    import detect
    detect.py()


def window():
    root.destroy()

#button2 = tk.Button(root, foreground="white", background="black", font=("Tempus Sans ITC", 14, "bold"),
#                   text="Data_Preprocessing", command=Data_Preprocessing, width=15, height=2)
#button2.place(x=5, y=90)

#button3 = tk.Button(root, foreground="white", background="black", font=("Tempus Sans ITC", 14, "bold"),
#                    text="Model Training", command=Model_Training, width=15, height=2)
#button3.place(x=5, y=170)

button4 = tk.Button(root, foreground="white", background="black", font=("Tempus Sans ITC", 14, "bold"),
                    text="Age And Gender Prediction", command=call_file, width=25, height=2)
button4.place(x=600, y=250)
exit = tk.Button(root, text="Exit", command=window, width=25, height=2, font=('times', 15, ' bold '),bg="Green",fg="white")
exit.place(x=600, y=330)

root.mainloop()

'''+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'''